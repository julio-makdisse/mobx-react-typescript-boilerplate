import { observable, computed, action, configure } from 'mobx'
import * as dayjs from 'dayjs'

// @ts-ignore
import * as ptBR from 'dayjs/locale/pt-br'
import IEstado from './IEstado'

configure({
  enforceActions: true,
})

export default class Estado implements IEstado {
  @observable
  public taxaAtualizacao: number = 500

  @observable
  public ligarPontoFlutuante: boolean = true

  @observable
  public estaLigado: boolean = false

  @observable
  public dataAtual: Date = new Date()

  @observable
  public dataInicial: Date = new Date()

  @computed
  get dataAtualFormatada() {
    return dayjs(this.dataInicial).format('dddd, MMMM D YYYY, HH:mm:ss')
  }

  private timeoutId: number = 0

  constructor() {
    dayjs.locale(ptBR)
    this.step()
  }

  @action
  step() {
    this.dataAtual = new Date()

    if (this.estaLigado) {
      this.timeoutId = setTimeout(() => {
        this.step()
      }, this.taxaAtualizacao)
    }
  }

  @action
  estaLigadoToggle() {
    this.estaLigado = !this.estaLigado

    if (this.timeoutId !== undefined) {
      clearTimeout(this.timeoutId)
    }

    if (this.estaLigado) {
      this.step()
    }
  }

  @action
  taxaAtualizacaoAlterar(novoValor: number) {
    this.taxaAtualizacao = novoValor
  }

  @action
  ligarPontoFlutuanteToggle() {
    this.ligarPontoFlutuante = !this.ligarPontoFlutuante
  }
}
