import styled from '@emotion/styled'

type BotaoProps = {
  ligado: boolean
}

const Botao = styled('button')<BotaoProps>`
  padding: 5px 15px;
  font-size: 35px;
  margin: 30px 0 0 0;
  box-shadow: 2px 2px 8px black;
  cursor: pointer;
  background-color: ${props => (props.ligado ? 'white' : 'gray')};
`

export default Botao
