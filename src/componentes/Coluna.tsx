import styled from '@emotion/styled'

const Coluna = styled('div')`
  display: flex;
  margin: 0 0 20px 0;
`

export default Coluna
