import * as React from 'react'
import Caixa from './Caixa'
import Hora from './Hora'
import * as dayjs from 'dayjs'
import IEstado from '../IEstado'
import { observer } from 'mobx-react'

@observer
class Relogio extends React.Component<
  {
    estadoMobx: IEstado
    formatacao?: string
  },
  {}
> {
  render() {
    const horaFormatada = dayjs(this.props.estadoMobx.dataAtual).format(
      this.props.formatacao || 'DD/MM/YYYY HH:mm:ss'
    )
    return (
      <Caixa>
        <Hora>{horaFormatada}</Hora>
      </Caixa>
    )
  }
}

export default Relogio
