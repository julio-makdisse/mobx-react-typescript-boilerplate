import styled from '@emotion/styled'

const Caixa = styled('div')`
  display: flex;
  flex-direction: row;
  align-items: baseline;
  color: white;
`

export default Caixa
