import * as React from 'react'
import styled from '@emotion/styled'
import { observer } from 'mobx-react'
import { observable, action } from 'mobx'

interface IProps {
  className?: string
}

@observer
class Componente extends React.Component<IProps, {}> {
  @observable valor: string = ''

  render() {
    return (
      <input
        className={this.props.className}
        value={this.valor}
        onChange={this.onChange.bind(this)}
      />
    )
  }

  @action onChange(e: { target: { value: string } }) {
    this.valor = e.target.value
  }
}

const NumeroInput = styled(Componente)`
  color: blue;
  font-size: 20px;
  padding: 5px 10px;
`

export default NumeroInput
