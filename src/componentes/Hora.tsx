import styled from '@emotion/styled'

const Hora = styled('div')`
  margin: 0 10px;
  color: hsl(183, 40%, 50%);
  background-color: hsl(174, 40%, 15%);
`

export default Hora
