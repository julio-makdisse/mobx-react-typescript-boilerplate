import styled from '@emotion/styled'

const Container = styled('div')`
  display: flex;
  flex-direction: column;
  align-content: stretch;
  font-family: monospace;
  height: 100%;
  padding: 30px;
  font-size: 30px;
  background-color: #444;
`

export default Container
