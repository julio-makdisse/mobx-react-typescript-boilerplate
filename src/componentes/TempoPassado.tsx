import * as React from 'react'
import Caixa from './Caixa'
import Hora from './Hora'
import * as dayjs from 'dayjs'
import IEstado from '../IEstado'
import { QUnitType } from 'dayjs'
import { observer } from 'mobx-react'

@observer
class TempoPassado extends React.Component<
  {
    estadoMobx: IEstado
    unidade?: QUnitType
  },
  {}
> {
  render() {
    const {
      dataInicial,
      dataAtual,
      ligarPontoFlutuante,
    } = this.props.estadoMobx
    const data1 = dayjs(dataInicial)
    const data2 = dayjs(dataAtual)
    return (
      <Caixa>
        <Hora>
          {Math.round(
            data2.diff(
              data1,
              this.props.unidade || 'second',
              ligarPontoFlutuante || false
            ) * 100000
          ) / 100000}
        </Hora>
      </Caixa>
    )
  }
}

export default TempoPassado
