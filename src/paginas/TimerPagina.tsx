import * as React from 'react'
import { observer } from 'mobx-react'
import Colunas from '../componentes/Colunas'
import Coluna from '../componentes/Coluna'
import Botao from '../componentes/Botao'
import Relogio from '../componentes/Relogio'
import TempoPassado from '../componentes/TempoPassado'
import Caixa from '../componentes/Caixa'
import IEstado from '../IEstado'
import NumeroInput from '../componentes/NumeroInput'

@observer
export default class TimerPagina extends React.Component<
  {
    estadoMobx: IEstado
  },
  {}
> {
  render() {
    return (
      <Colunas>
        <Coluna>
          Agora são:
          <Relogio estadoMobx={this.props.estadoMobx} formatacao="HH:mm:ss" />
        </Coluna>
        <Coluna>
          Já se passaram:
          <TempoPassado estadoMobx={this.props.estadoMobx} unidade="hour" />
          horas
        </Coluna>
        <Coluna>
          ou:
          <TempoPassado estadoMobx={this.props.estadoMobx} unidade="minute" />
          minutos
        </Coluna>
        <Coluna>
          ou:
          <TempoPassado estadoMobx={this.props.estadoMobx} />
          segundos
        </Coluna>

        <Coluna>
          Timer:
          <Botao
            onClick={() => this.props.estadoMobx.estaLigadoToggle()}
            ligado={this.props.estadoMobx.estaLigado}
          >
            {this.props.estadoMobx.estaLigado ? 'Ligado' : 'Desligado'}
          </Botao>
        </Coluna>
        <Coluna>
          Ponto Flutuante:
          <Botao
            onClick={() => this.props.estadoMobx.ligarPontoFlutuanteToggle()}
            ligado={this.props.estadoMobx.ligarPontoFlutuante}
          >
            {this.props.estadoMobx.ligarPontoFlutuante ? 'Ligado' : 'Desligado'}
          </Botao>
        </Coluna>
        <Coluna>
          Taxa de atualização: {this.props.estadoMobx.taxaAtualizacao}ms
        </Coluna>
        <Coluna>
          <input
            style={{ width: '490px' }}
            type="range"
            onChange={el =>
              this.props.estadoMobx.taxaAtualizacaoAlterar(
                parseFloat(el.target.value)
              )
            }
            step={100}
            min={0}
            max={2000}
            value={this.props.estadoMobx.taxaAtualizacao}
          />
        </Coluna>
        <Coluna>
          <Caixa>{this.props.estadoMobx.dataAtualFormatada}</Caixa>
        </Coluna>
        <Coluna>
          <NumeroInput />
        </Coluna>
      </Colunas>
    )
  }
}
