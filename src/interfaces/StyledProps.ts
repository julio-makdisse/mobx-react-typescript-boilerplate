type StyledProps<T = {}, U = {}> = React.DetailedHTMLProps<
  React.HTMLAttributes<U> & Partial<T>,
  U
>

export default StyledProps
