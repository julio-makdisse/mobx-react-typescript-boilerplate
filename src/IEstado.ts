export default interface IEstado {
  taxaAtualizacao: number
  ligarPontoFlutuante: boolean
  estaLigado: boolean
  dataAtual: Date
  dataInicial: Date
  dataAtualFormatada: string
  step: () => void
  estaLigadoToggle: () => void
  taxaAtualizacaoAlterar: (novaTaxa: number) => void
  ligarPontoFlutuanteToggle: () => void
}
