import * as React from 'react'
import * as ReactDOM from 'react-dom'
import DevTools from 'mobx-react-devtools'

import EstadoMobx from './EstadoMobx'
import TimerPagina from './paginas/TimerPagina'

const estadoMobx = new EstadoMobx()

ReactDOM.render(
  <div>
    <TimerPagina estadoMobx={estadoMobx} />,
    <DevTools />
  </div>,
  document.getElementById('root')
)
